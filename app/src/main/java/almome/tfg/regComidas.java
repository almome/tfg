package almome.tfg;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import almome.tfg.FeedReaderAlimentos.FeedEntry;

import static almome.tfg.FeedReaderAlimentos.FeedEntry.TABLE_NAME;





public class regComidas extends AppCompatActivity {

    private ImageButton editFech, editHora;
    private TextView textVFecha, textVHora, textVCons;
    private DatePicker datePicker;
    private Calendar c;
    private int year, month, day, hor, min;
    private EditText plato;
    private Button ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_comidas);

        editFech = (ImageButton) findViewById(R.id.editFecha); //Boton de editar fecha
        editHora = (ImageButton) findViewById(R.id.editHora); //Boton de editar hora
        textVFecha = (TextView) findViewById(R.id.tVifecha); //Muestra fecha
        textVHora = (TextView) findViewById(R.id.tVihora); //Muestra hora
        c = Calendar.getInstance(); //Calendario
        plato = (EditText) findViewById(R.id.nombrePlato);
        ok = (Button) findViewById(R.id.okbtn);
        textVCons = (TextView) findViewById(R.id.textView);

        //Tomar hora y fecha actuales
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat dh = new SimpleDateFormat("hh:mm");
        String formattedDate = df.format(c.getTime());
        String formattedHour = dh.format(c.getTime());

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hor = c.get(Calendar.HOUR);
        min = c.get(Calendar.MINUTE);

        textVFecha.setText(formattedDate);
        textVHora.setText(formattedHour);

        /*--------------------- BD -----------------------*/
        //Se crea BD
        FeedReaderDbHelper mDbHelper = new FeedReaderDbHelper(getBaseContext());
        //Se obtiene la BD en modo escritura
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        //Se crea nuevo mapa de valores, donde los nombres de las columnas son las claves
        ContentValues values = new ContentValues();
        values.put(FeedEntry.COLUMN_NAME_FECHA, "Fecha");
        values.put(FeedEntry.COLUMN_NAME_HORA, "Hora");
        values.put(FeedEntry.COLUMN_NAME_NPLATO, "NombrePLato");
        values.put(FeedEntry.COLUMN_NAME_LACTOSA, "Lactosa");
        values.put(FeedEntry.COLUMN_NAME_HUEVO, "Gluten");
        values.put(FeedEntry.COLUMN_NAME_PESCADO, "Pescado");
        values.put(FeedEntry.COLUMN_NAME_MARISCO, "Marisco");
        values.put(FeedEntry.COLUMN_NAME_FCASCARA, "FrutoCascara");
        values.put(FeedEntry.COLUMN_NAME_LEGUMBRES, "Legumbre");
        values.put(FeedEntry.COLUMN_NAME_CEREALES, "Cereal");
        values.put(FeedEntry.COLUMN_NAME_FRUTA, "Fruta");
        values.put(FeedEntry.COLUMN_NAME_CONSAB, "ConservSabor");

        long newRowId = db.insert(TABLE_NAME, null, values);

        Log.d("db", "ID: " + String.valueOf(newRowId));
        Log.d("db", "Path: " + db.getPath());

        db.close();

        db = mDbHelper.getReadableDatabase();

        //Definimos las proyecciones que especifican cuales columnas vamos a usar
        String[] projection = {
                FeedEntry._ID,
                FeedEntry.COLUMN_NAME_NPLATO,
                FeedEntry.COLUMN_NAME_FRUTA
        };
        // Filter results WHERE "title" = 'Title'
        String selection = FeedEntry.COLUMN_NAME_NPLATO + " = ?";
        String[] selectionArgs = { "NombrePLato" };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = FeedEntry._ID + " DESC";

        Cursor cursor = db.query(
                FeedEntry.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        cursor.moveToFirst();
        int row = 0;
        for( ; row < cursor.getCount(); ++row) {
            Log.d("db", cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry._ID)) + " " +
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_NAME_NPLATO)) + " "+
                    cursor.getString(cursor.getColumnIndexOrThrow(FeedEntry.COLUMN_NAME_FRUTA)));
            cursor.moveToNext();
        }










        ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(regComidas.this, mainMenu.class);
                startActivity(intent);
            }
        });

    }

    //***************DIALOGO_FECHA**************

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        if (id == 998) {
            return new TimePickerDialog(this, myTimeListener, hor, min, true);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
        textVFecha.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    //***************DIALOGO_HORA************

    @SuppressWarnings("deprecation")
    public void setTime (View view) {
        showDialog(998);
        Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }

    private TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker arg0, int arg1, int arg2) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showTime(arg1, arg2);
        }
    };

    private void showTime(int hor, int min) {
        textVHora.setText(new StringBuilder().append(hor).append(":")
                .append(min));
    }
}
