package almome.tfg;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

/* Pantalla de inicio de la aplicación
    - Título de la aplicación y pequeño mensaje de lo que es.
    - Fondo diseñado.
 */

public class portada extends AppCompatActivity {

    private LinearLayout linearL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portada);

        linearL = (LinearLayout)findViewById(R.id.activity_portada);

        linearL.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Creamos el Intent
                Intent intent = new Intent(portada.this, mainMenu.class);
                //Iniciamos la nueva actividad
                startActivity(intent);
            }
        });
    }


}
