package almome.tfg;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class mainMenu extends AppCompatActivity {

    /* Menú principal
    - Registrar comida.
    - Me encuentro mal.
    - Top de alimentos que peor sientan.

    ATENCIÓN: HACER QUE CUANDO SE PULSE 'ATRÁS' NO VUELVA A LA PORTADA
 */

    private Button btnRegisCom;
    private Button btnRegisMal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        btnRegisCom = (Button)findViewById(R.id.botonRegisCom);
        btnRegisMal = (Button)findViewById(R.id.botonMalestar);

        btnRegisCom.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v){
                        Intent intent = new Intent(mainMenu.this, regComidas.class);
                        startActivity(intent);
                    }
                }
        );

        btnRegisMal.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v) {
                        Intent intent = new Intent(mainMenu.this, malestar.class);
                        startActivity(intent);
                    }
                }
        );
    }
}
