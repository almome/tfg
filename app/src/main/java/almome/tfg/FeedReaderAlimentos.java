package almome.tfg;


import android.provider.BaseColumns;
/**
 * Created by sandra on 5/02/17.
 */

public final class FeedReaderAlimentos {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private FeedReaderAlimentos() {}

    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "alimentos";
        public static final String COLUMN_NAME_FECHA = "fecha";
        public static final String COLUMN_NAME_HORA = "hota";
        public static final String COLUMN_NAME_NPLATO = "nombre_plato";
        public static final String COLUMN_NAME_LACTOSA = "lactosa";
        public static final String COLUMN_NAME_GLUTEN = "gluten";
        public static final String COLUMN_NAME_HUEVO = "huevo";
        public static final String COLUMN_NAME_PESCADO = "pescado";
        public static final String COLUMN_NAME_MARISCO = "marisco";
        public static final String COLUMN_NAME_FCASCARA = "frutos_cascara";
        public static final String COLUMN_NAME_LEGUMBRES = "legumbres";
        public static final String COLUMN_NAME_CEREALES = "cereales";
        public static final String COLUMN_NAME_FRUTA = "fruta";
        public static final String COLUMN_NAME_CONSAB = "conervante_saborizante";

    }

}
