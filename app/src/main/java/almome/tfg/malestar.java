package almome.tfg;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


public class malestar extends AppCompatActivity {
    private Button aceptaBtn;
    private ImageButton editFech, editHora;
    private TextView textVFecha, textVHora;
    private DatePicker datePicker;
    private Calendar c;
    private int year, month, day, hor, min;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_malestar);

        aceptaBtn = (Button) findViewById(R.id.aceptarBtn); //Boton de registrar
        editFech = (ImageButton) findViewById(R.id.editarFecha); //Boton de editar fecha
        editHora = (ImageButton) findViewById(R.id.editarHora); //Boton de editar hora
        textVFecha = (TextView) findViewById(R.id.tVFecha); //Muestra fecha
        textVHora = (TextView) findViewById(R.id.tVHora); //Muestra hora
        c = Calendar.getInstance(); //Calendario

        //Tomar hora y fecha actuales
        SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
        SimpleDateFormat dh = new SimpleDateFormat("hh:mm");
        String formattedDate = df.format(c.getTime());
        String formattedHour = dh.format(c.getTime());

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hor = c.get(Calendar.HOUR);
        min = c.get(Calendar.MINUTE);

        textVFecha.setText(formattedDate);
        textVHora.setText(formattedHour);


        aceptaBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(malestar.this, mainMenu.class);
                startActivity(intent);
            }
        });
    }

    //***************DIALOGO_FECHA*******************

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        if (id == 998) {
            return new TimePickerDialog(this, myTimeListener, hor, min, true);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2 + 1, arg3);
        }
    };

    private void showDate(int year, int month, int day) {
        textVFecha.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    //***************DIALOGO_HORA*******************

    @SuppressWarnings("deprecation")
    public void setTime (View view) {
        showDialog(998);
        Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT).show();
    }

    private TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker arg0, int arg1, int arg2) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showTime(arg1, arg2);
        }
    };

    private void showTime(int hor, int min) {
        textVHora.setText(new StringBuilder().append(hor).append(":")
                .append(min));
    }




}

