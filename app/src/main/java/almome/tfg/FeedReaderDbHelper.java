package almome.tfg;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import almome.tfg.FeedReaderAlimentos.FeedEntry;

public class FeedReaderDbHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "FeedReader.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN_NAME_FECHA + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_HORA + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_NPLATO + TEXT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_LACTOSA + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_GLUTEN + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_HUEVO + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_PESCADO + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_MARISCO + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_FCASCARA + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_LEGUMBRES + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_CEREALES + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_FRUTA + INT_TYPE + COMMA_SEP +
                    FeedEntry.COLUMN_NAME_CONSAB + INT_TYPE + " )";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    public FeedReaderDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
